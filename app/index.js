require('dotenv').config({
	silent: true
});
const express = require('express')
const app = express()
const server = require('http').Server(app);
const bodyParser = require('body-parser');
const parseForm = bodyParser.urlencoded({
	limit: '50mb',
	extended: false
})
const parseJson = bodyParser.json({
	limit: '50mb'
})
const _ = require('lodash');
const pug = require('pug');
const fs = require('fs');
const PORT = process.env.PORT || 3000;
const mongoose = require('mongoose');
const path = require('path');
const VIEWS_BASE_DIR = __dirname
const sander = require('sander')
const requireFromString = require('require-from-string');
const cors = require('cors')
const uniqid = require('uniqid');
const require_install = require('require-install');
const sequential = require('promise-sequential');

var routeErrors = []

var exceptions = []
if (process.env.WRAPKEND_DB_DIRECT === '1') {
	exceptions.push('WRAPKEND_SOCKET_URI')
}

if (!!getMissingEnv(exceptions)) {
	console.error(`ERROR: ${getMissingEnv()} required`)
	return exitInSeconds(30)
}

var isCoreProject = process.env.WRAPKEND_DB_DIRECT === '1'

var SOCKET_TIMEOUT_WAIT_START = 10 * 1000
const SOCKET_TIMEOUT_WAIT_END = 60 * 1000

var EDITOR_SOCKET_CONNECTION_TIMEOUT = 20 * 1000
if (process.env.WRAPKEND_DB_DIRECT === '1') {
	EDITOR_SOCKET_CONNECTION_TIMEOUT = 60 * 1000 * 2
}

function rimraf(cmd) {
	return new Promise((resolve, reject) => {
		app.requireInstall('rimraf')(cmd, (err) => {
			if (err) reject(err)
			else resolve()
		})
	})
}

app.rimraf = rimraf

app._server = server
app.requireInstall = require_install
app.getMongooseModel = getMongooseModel
app.setMongooseModel = setMongooseModel
var cache = {};
app.data = scope = {
	logged: false,
	views: {},
	mongooseModels: {}
}

const Wrapkend = {
	getProjectFilesByType: t => app.wrapkendCall('getProjectFilesByType', t),
	getProject: t => app.wrapkendCall('getProject', t),
	getFileById: t => app.wrapkendCall('getFileById', t)
}
app.wrapkend = app.Wrapkend = Wrapkend

configureAppHelpers(app)

mongoose.set('debug', process.env.MONGOOSE_DEBUG === '0' ? false : true);

return init().catch(err => {
	console.error('ERROR: INIT FAIL', err.stack)
	return exitInSeconds(10)
})

async function pingSocketServer() {
	if (isCoreProject) {
		return;
	}
	try {
		let wakeUpData = await require('axios').post(process.env.WRAPKEND_SOCKET_URI + '/rpc/wakeUpProjectSocket', {
			privateKey: process.env.WRAPKEND_APIKEY
		})
		scope.wakeUpData = wakeUpData
		scope.socketOnline = true
	} catch (err) {
		console.log('ERROR', 'pingSocketServer', err.stack)
	}
	setTimeout(() => pingSocketServer(), 1000 * 15)

}

async function pingSocketState() {
	if (isCoreProject) {
		return;
	}
	var socket = app.data.clientSocket
	console.log('Socket alive', socket && socket.connected)
	if (!(socket && socket.connected)) {
		console.log('Re-connecting...')

		await pingSocketServer()

		setTimeout(() => {
			socket && socket.open()
			setTimeout(() => pingSocketState(), 1000 * 10)
		}, 5000)

	} else {
		setTimeout(() => pingSocketState(), 1000 * 5)
	}

}

async function init(opts = {}) {

	//return migrateProjects();

	if (!isCoreProject) {
		if (!scope.socketOnline) {
			if (!opts.socketCalled) {
				await pingSocketServer()
				return setTimeout(() => init({
					socketCalled: true
				}), 1000)
			} else {
				console.log('Waiting Wrapkend socket... (wc2 is up?)')
				return setTimeout(() => init({
					socketCalled: true
				}), 5000)
			}
		}
	} else {

	}

	//setTimeout(() => pingSocketServer(), 1000 * 30)
	setTimeout(() => pingSocketState(), 1000 * 5)

	if(!isCoreProject){
		await configureIOServer(process.env.WRAPKEND_SOCKET_URI + '/' + scope.wakeUpData.data)
	}
	

	await configureDatabase()
	await configureMiddlewares()

	await configureDynamicMiddlewares()
	await configureFunctions()
	await configureServices();
	await configureDynamicRoutes()
	await configureDynamicViews()

	app.use('/', express.static(path.join(process.cwd(), 'app/assets')));

	server.listen(PORT, function() {
		console.log(`Listening on ${process.env.VIRTUAL_HOST}:${PORT}`)
	})
}

async function getProjectFilesByType(type) {
	let pr = await app.getProject()
	return getMongooseModel('file').find({
		_id: {
			$in: pr.files
		},
		type: {
			$in: type
		}
	})
}

async function configureIOServer(clientUri) {
	return new Promise((resolve, reject) => {
		var socketTimeout = null
		var resolved = false;
		const io = require('socket.io')(server);
		app.io = io
		if (process.env.WRAPKEND_DB_DIRECT === '1') {
			console.log('Wrapkend: configureIOServer OK')
			resolved = true;
			resolve()
		}
		io.on('connection', function(socket) {
			console.log('Socket client connected', socket.id)
			socket.on('wrapkendCall', params => {
				(async () => {
					if (!params.id) {
						return console.log('Socket: Incoming ignored (no id)')
					}
					params.p = params.p || {}
					socket.emit('wrapkendCall_' + params.id + '_working')
					var result = {
						err: "NO_IMPLEMENTED"
					}
					try {
						if (!params.k) {
							throw new Error('NO_ALLOWED_INVALID_KEY')
						}

						let pr = await getMongooseModel('project').findOne({
							privateKey: params.k
						}).exec()
						if (!pr) {
							throw new Error('NO_ALLOWED_PROJECT_NOT_FOUND')
						}
						if (params.n === 'getProjectFilesByType') {
							result = await getProjectFilesByType(params.p, pr)
						}
						if (params.n === 'getProject') {
							result = pr.toJSON()
						}
						if (params.n === 'getFileById') {
							result = await getFileById(params.p)
						}
					} catch (err) {
						console.error('ERROR', 'Incoming socket fn request', err.stack)
						result = {
							err: err.stack
						}
					}
					try {
						socket.emit('wrapkendCall_' + params.id, result)
					} catch (err) {
						console.error('ERROR', 'When sending socket fn request result', err.stack)
						socket.emit('wrapkendCall_' + params.id, {
							err: err.stack
						})
					}
				})();
			})
		});
		io.on('foo', () => console.log('foo'))



		var socket
		//socket = require('socket.io-client')(process.env.WRAPKEND_SOCKET_URI);
		socket = require('socket.io-client')(clientUri);
		socket.on('connect', function() {
			console.log('CONNECTED to HOST', clientUri)
			if (!resolved) {
				console.log('Wrapkend: configureIOServer OK')
				resolved = true
				resolve()
			}
			clearTimeout(socketTimeout)
		});
		socket.on('event', function(data) {});
		socket.on('disconnect', function() {});
		socket.on('save-file', async function(p) {
			var pr = await app.getProject();
			if (p && p.prs && p.prs.length > 0 && p.prs.find(s => s.toString() == pr._id.toString()) == null) {
				return console.log('ANOTHER PR SAVE FILE, IGNORE', pr._id, pr.name, 'prs', p.prs, );
			}

			let f = await getFileById(p._id)
			if (['function', 'pug', 'css', 'javascript'].includes(f.type)) {
				console.log('Live Update', f.name, f.type)
				if (f && f.type === 'function') {
					return loadFunction(f)
				}
				if (f && f.type === 'pug') {
					return loadPugFile(f);
				}

				//those files don't need to be reloaded (e.g css)
				return;
			}

			console.log('SAVE FILE, EXIT')
			process.exit(0)
		});
		app.data.clientSocket = socket



		app.wrapkendCall = (n, p) => {
			console.log('Wrapkend call', n)
			return new Promise((resolve, reject) => {
				let id = uniqid()
				let timeoutStart = false
				let timeoutEnd = false
				let timeoutStartTimeout = setTimeout(() => {
					timeoutStart = true
					reject(new Error('TIMEOUT_START'))
				}, SOCKET_TIMEOUT_WAIT_START)
				socket.once('wrapkendCall_' + id + '_working', (rta) => {
					clearTimeout(timeoutStartTimeout)
					setTimeout(() => {
						timeoutEnd = true
						reject(new Error('TIMEOUT_END'))
					}, SOCKET_TIMEOUT_WAIT_END)
				})
				socket.once('wrapkendCall_' + id, (rta) => {
					if (!timeoutStart && !timeoutEnd) {
						if (rta.err) {
							reject(rta.err)
						} else {
							resolve(rta)
						}
					}
				})
				socket.emit('wrapkendCall', {
					k: process.env.WRAPKEND_APIKEY,
					p,
					n,
					id
				});
			})
		}

		socketTimeout = setTimeout(() => {
			console.error('ERROR Socket connection timeout')
			exitInSeconds(0)
		}, EDITOR_SOCKET_CONNECTION_TIMEOUT)
		console.log('Wrapkend: Looking editor socket at', process.env.WRAPKEND_SOCKET_URI)
	})
}

async function migrateProjects() {
	await configureDatabase()
	let cores = await getMongooseModel('project').find({}).exec()
	await sequential(cores.map(c => {
		return async () => {
			var pr = await getMongooseModel('project').findOne({
				name: c.name
			}).exec()
			if (!pr) {
				pr = await getMongooseModel('project').create({
					name: c.name
				})
				await setProjectDefaults(pr, "5b5e41fca7928aafac0f3537")
			}
			let files = (c.files || []).concat(pr.files || [])
			files = files.filter(f => f != null).map(f => f.toString())
			files = files.filter(function(item, pos) {
				return files.indexOf(item) == pos;
			})
			pr.files = files
			await pr.save()
			console.log(`Project ${c.name} migrated`)
		}
	}))

	//search inside all files and replace project with project
	let files = await getMongooseModel('file').find({}).exec()
	await sequential(files.map(f => {
		return async () => {
			f.code = f.code.split('project').join('project')
			await f.save()
		}
	}))

	console.log('migrateProjects done')
	exitInSeconds(30)
}

async function setProjectDefaults(pr, userId) {
	if (!pr) return
	pr.users = pr.users || []
	if (!pr.users.find(u => u.toString() == userId.toString())) {
		pr.users.push(userId)
		if (!pr.usersRights) {
			pr.usersRights = {}
		}
		pr.usersRights[userId] = 'owner'
	}
	pr.description = pr.description || ''
	pr.settings = pr.settings || {}
	if (!pr.settings.envs) {
		var PORT = await findFreePort(pr._id)
		pr.settings.envs = {
			production: {
				NODE_ENV: 'production',
				PORT,
				VIRTUAL_PORT: PORT
			},
			development: {
				NODE_ENV: 'development'
			}
		}
	}

	if (pr.domain) {
		pr.settings.envs.production.VIRTUAL_HOST = pr.domain
		pr.settings.envs.production.LETSENCRYPT_HOST = pr.domain
	}
	pr.settings.envs.production.SOCKET_URI = "https://editor.wrapkend.com"
	pr.settings.envs.production.LETSENCRYPT_EMAIL = "arancibiajav@gmail.com"
	if (!pr.privateKey) {
		pr.privateKey = app.requireInstall('uniqid')('wrapkend-')
	}
	if (pr.privateKey) {
		pr.settings.envs.production.API_KEY = pr.privateKey
	}

	await pr.save()
}

function getMongooseModel(name) {
	if (!scope.mongooseModels[name]) {
		console.error(`ERROR: Mongoose model ${name} not found `)
		return exitInSeconds(30)
	}
	//console.log('getMongooseModel', name, scope.mongooseModels[name])
	return mongoose.model(scope.mongooseModels[name])
}

function setMongooseModel(name, def) {
	let id = uniqid()
	scope.mongooseModels[name] = id
	//console.log('setMongooseModel', name, id)
	mongoose.model(id, def)
	try {
		mongoose.model(name)
	} catch (err) {
		mongoose.model(name, def)
	}
}

function getMissingEnv(ignore = []) {
	console.log('getMissingEnv', 'ignore', ignore)
	var envs = ['VIRTUAL_HOST', 'LETSENCRYPT_HOST', 'VIRTUAL_PORT', 'LETSENCRYPT_EMAIL',
		'NODE_ENV', 'WRAPKEND_SOCKET_URI', 'PORT', 'WRAPKEND_SOCKET_URI', 'WRAPKEND_APIKEY', 'WRAPKEND_DB_DIRECT'
	]
	var n = ''
	for (var x in envs) {
		if (process.env[envs[x]] == undefined) {
			if (ignore.includes(envs[x])) {
				continue;
			}
			n = envs[x]
		}
	}
	return n
}

function exitInSeconds(s) {
	setTimeout(() => process.exit(0), 1000 * s)
}



function configureAppHelpers(app) {

	app.getProject = async (apiKey, project) => {
		if (process.env.WRAPKEND_DB_DIRECT != '1') {
			return await Wrapkend.getProject(process.env.WRAPKEND_APIKEY)
		}
		var doc = await getMongooseModel('project').findOne({
			privateKey: process.env.WRAPKEND_APIKEY
		}).exec()
		if (!doc) {
			console.error(`ERROR: project not found (WRAPKEND_APIKEY is ${process.env.WRAPKEND_APIKEY})`)
		}
		return doc
	}

	app.lazyFn = (n) => {
		return async (req, res, next) => {
			return app.fn[n](req, res, next)
		}
	}
	app.wait = (seconds) => {
		return new Promise((resolve, reject) => {
			setTimeout(() => resolve(), seconds * 1000)
		})
	}
	app.waitForFunction = async (n) => {
		if (app.function && app.function[n]) {
			return
		} else {
			for (var x = 1; x <= 20; x++) {
				await app.wait(1)
				if (app.function && app.function[n]) {
					return
				}
			}
		}
	}
	app.waitForService = async (n) => {
		if (app.service && app.service[n]) {
			return
		} else {
			for (var x = 1; x <= 20; x++) {
				await app.wait(1)
				if (app.service && app.service[n]) {
					return
				}
			}
		}
	}

}


async function getFileById(id) {
	if (process.env.WRAPKEND_DB_DIRECT != '1') {
		return await Wrapkend.getFileById(id)
	} else {
		return await getMongooseModel('file').findById(id).exec()
	}
}
async function getFilesByType(types) {
	console.log('getFilesByType', types)
	if (process.env.WRAPKEND_DB_DIRECT != '1') {
		return await Wrapkend.getProjectFilesByType(types)
	} else {
		return await getProjectFilesByType(types)
	}
}
async function getFunction(name) {
	var pr = await app.getProject();
	var conditions = {
		_id: {
			$in: pr.files
		},
		type: {
			$in: ['rpc', 'function']
		},
		name
	};
	return await getMongooseModel('file').findOne(conditions).exec()
}

function loadPugFile(file) {
	try {
		file.name = file.name.split('.')[0]
		app.data.views[file.name] = file.code
		sander.writeFileSync(path.join(process.cwd(), 'app/views', file.name + '.pug'), file.code)
		return true;
	} catch (err) {
		console.error('ERROR', 'Unable to load view', file.name, err.stack)
		return false;
	}
}



function configureDynamicViews() {
	return new Promise(async (resolve, reject) => {
		try {
			await rimraf(path.join(process.cwd(), 'app/views/*'))
			getFilesByType(['pug']).then(res => {
				let okCount = 0
				res.forEach(file => {
					if (loadPugFile(file)) {
						okCount++
					}
				})
				console.log(`Wrapkend: configureDynamicViews OK (${okCount}/${res.length})`)
				resolve()
			})
		} catch (err) {
			console.error('ERROR', err.stack)
			console.log('Wrapkend: configureDynamicViews FAIL')
		}
	})
}


function configureDynamicMiddlewares() {
	return new Promise((resolve, reject) => {
		try {

			getFilesByType(['middleware']).then(res => {
				var okCount = 0
				res.forEach(file => {
					try {
						var mod = requireFromString(file.code)
						var middleware = mod(app)
						if (middleware instanceof Promise) {
							middleware.then(impl => {
								app.use(impl)
							}).catch(err => console.error(err.stack))
						} else {
							app.use(middleware)
						}
						okCount++
					} catch (err) {
						console.log('ERROR (Middleware)', file.name, err.stack)
						console.error(err.stack)
					}
				})
				console.log(`Wrapkend: configureDynamicMiddlewares OK (${okCount}/${res.length})`)
				resolve()
			})
		} catch (err) {
			console.error('ERROR', err.stack)
			console.log('Wrapkend: configureDynamicMiddlewares FAIL')
		}
	})
}

await

function configureMiddlewares() {

	app.use('*', (req, res, next) => {
		if (routeErrors.length > 0) {
			return res.send(routeErrors.join("\n"))
		} else {
			next()
		}
	})


	app.use((req, res, next) => {
		req.logged = app.data.logged || false;
		res.sendView = (name, data = {}) => {
			try {
				res.send(compileFileWithVars(name, data, req));
			} catch (err) {
				console.log('ERROR', err.stack);
				res.status(500).send(err.stack);
			}
		}
		next();
	});

	app.use(parseJson, (req, res, next) => {
		console.log('REQ', req.method, req.url, Object.keys(req.body).map(k => k + (!req.body ? ':Empty' : '')).join(', '))
		next();
	})
}



async function loadFunction(file) {
	try {
		var mod = requireFromString(file.code)
		if (!app.fn) {
			app.fn = {}
		}
		let impl = mod(app)
		if (impl instanceof Promise) {
			app.fn[file.name] = await impl
		} else {
			app.fn[file.name] = impl
		}

		let fn = app.fn[file.name]
		app.fn[file.name] = async function(){
			try{
				let w = fn.apply(this, arguments)
				if(w instanceof Promise){
					w = await w
				}
				return w;
			}catch(err){
				console.log('ERROR (Function)',err.stack)
				throw err
			}
		}

		return true;
	} catch (err) {
		console.error('ERROR', 'Unable to load function', file.name, err.stack)
		return false;
	}
}

function configureFunctions() {
	return new Promise((resolve, reject) => {
		try {
			getFilesByType(['function']).then(res => {
				var okCount = 0
				sequential(res.map(file => {
					return async () => {
						if (loadFunction(file)) {
							okCount++
						}
					}
				})).then(() => {
					console.log(`Wrapkend: configureFunctions OK (${okCount}/${res.length})`)
					resolve()
				})
			})
		} catch (err) {
			console.log('Wrapkend: configureFunctions FAIL')
			reject(err.stack)
		}
	})
}

async function configureServices() {
	let res = await getFilesByType(['service'])
	let okCount = 0

	await sequential(res.map(file => {
		return async () => {
			try {
				var name, impl

				var requireImpl = requireFromString(file.code)
				if (requireImpl instanceof Array) {
					name = requireImpl[0]
					impl = requireImpl[1]
				} else {
					name = file.name
					impl = requireImpl
				}

				if (!app.service) {
					app.service = {}
				}
				if (!app.srv) {
					app.srv = {}
				}

				if (typeof impl === 'function') {
					let result = impl(app)
					if (result instanceof Promise) {
						result = await result
					}
					app.service[name] = app.srv[name] = result
				} else {
					app.service[name] = app.srv[name] = impl
				}

				okCount++
			} catch (err) {
				console.error('ERROR', 'Unable to load service', file.name, err.stack)
			}
		}
	}))
	console.log(`Wrapkend: configureServices OK (${okCount}/${res.length})`)

}

function waitForFile(fullPath) {
	return new Promise((resolve, reject) => {
		check()

		function check() {
			if (!sander.existsSync(fullPath)) {
				setTimeout(check, 100)
			} else {
				resolve()
			}
		}
	})
}

async function configureDynamicRoutes() {
	let errors = []
	res = await getFilesByType(['route'])
	let okCount = 0
	await rimraf(path.join(process.cwd(), 'app/route/*'))
	await sequential(res.map(file => {
		return async () => {
			try {
				let filePath = path.join(process.cwd(), 'app', file.type, file.name + '.js')
				await sander.writeFile(filePath, file.code)
				console.log('wait', filePath)
				await waitForFile(filePath)
				var mod = require(filePath)
				//var mod = requireFromString(file.code)
				mod(app)
				okCount++
			} catch (err) {
				errors.push(['ERROR', 'Unable to load route', file.name, err.stack].join(' '))
				console.error('ERROR', 'Unable to load route', file.name, err.stack)
			}
		}
	}))
	console.log(`Wrapkend: configureDynamicRoutes OK (${okCount}/${res.length})`)

	if (errors.length > 0) {
		routeErrors = errors;
	} else {
		routeErrors = []
	}
}



function handleError(err, res, status = 500) {
	console.error(err);
	res.status(status).json(err.stack);
}


function configureDatabase() {
	return new Promise(async (resolve, reject) => {

		const WRAPKEND_DB_URI = process.env.WRAPKEND_DB_URI
		if (!WRAPKEND_DB_URI) {
			console.error('WRAPKEND_DB_URI required')
			process.exit(0)
		}

		try {
			await mongoose.connect(WRAPKEND_DB_URI, {
				server: {
					reconnectTries: Number.MAX_VALUE,
					reconnectInterval: 1000
				}
			})
		} catch (err) {
			console.error('ERROR Mongoose', err.stack)
		}

		if (process.env.WRAPKEND_DB_DIRECT === '1') {



			const schema = new mongoose.Schema({
				name: {
					type: String,
					required: true,
					unique: true,
					index: true
				},
				path: String,
				tags: [String],
				type: {
					type: String,
					required: true,
					//enum: ['view', 'function', 'route', 'vueComponent']
					//javascript, vueComponent, route, view, function, middleware, schedule)
				},
				code: {
					type: String,
					required: true
				}
			}, {
				timestamps: true,
				toObject: {},
				collection: 'files'
			});
			setMongooseModel('file', schema);



			setMongooseModel('project', new mongoose.Schema({
				name: {
					type: String,
					required: true,
					unique: true,
					index: true
				},
				envs: Object,
				files: [{
					type: mongoose.Schema.Types.ObjectId,
					ref: 'file'
				}],
				users: [{
					type: mongoose.Schema.Types.ObjectId,
					ref: 'cloud_user'
				}],
			}, {
				timestamps: true,
				toObject: {},
				collection: 'projects'
			}));

			setMongooseModel('package', new mongoose.Schema({
				name: {
					type: String,
					required: true,
					unique: true,
					index: true
				},
				project: {
					type: mongoose.Schema.Types.ObjectId,
					ref: 'project',
					index: true
				},
				files: [{
					type: mongoose.Schema.Types.ObjectId,
					ref: 'file'
				}]
			}, {
				timestamps: true,
				toObject: {},
				collection: 'packages'
			}));


			setMongooseModel('project', new mongoose.Schema({
				name: {
					type: String,
					required: true,
					unique: true,
					index: true
				},
				label: {
					type: String,
				},
				domain: {
					type: String
				},
				privateKey: {
					type: String,
					index: true
				},
				users: [{
					type: mongoose.Schema.Types.ObjectId,
					ref: 'cloud_user',
					index: true
				}],
				usersRights: Object,
				files: [{
					type: mongoose.Schema.Types.ObjectId,
					ref: 'file'
				}],
				description: {
					type: String,
					default: ''
				},
				settings: {
					type: Object,
					default: {}
				},
			}, {
				timestamps: true,
				toObject: {},
				collection: 'projects'
			}));
		}


		getFilesByType(['schema']).then(res => {
			var okCount = 0
			res.forEach(file => {
				try {
					var mod = requireFromString(file.code)
					mod(mongoose, app)
					okCount++
				} catch (err) {
					console.error(err.stack)
				}
			})
			resolve();
			console.log(`Wrapkend: configureDatabase (${okCount}/${res.length})`)
		}).catch(reject)
	})
}


function compileFileWithVars(filePath, vars = {}, req) {
	var p = path.join(VIEWS_BASE_DIR, 'views', filePath.replace('.pug', '') + '.pug')
	if (sander.existsSync(p)) {
		return pug.compileFile(p)(vars)
	} else {
		return "File not found: " + p
	}
}


function compileCode(code, browser = false, opts = {
	minified: false,
	type: 'javascript'
}) {
	if (!['javascript', 'component'].includes(opts.type)) {
		return code;
	}
	var targets = {
		"node": "6.0"
	};
	if (browser) {
		delete targets.node;
		//targets.browsers = ["5%", "last 2 versions", "Firefox ESR", "not dead"];
		targets.chrome = '30';
	}
	return require('babel-core').transform(code, {
		minified: opts.minified,
		babelrc: false,
		sourceMaps: 'inline',
		presets: [
			["env", {
				"targets": targets
			}]
		]
	}).code;
}
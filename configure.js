const argv = require('yargs').argv
const sander = require('sander')
var f = sander.readFileSync(process.cwd() + '/docker-compose.yaml.tpl').toString('utf-8')
f = f.replace('__container__', argv.name.split('.').join('_')).split('__port__').join(argv.port)
sander.writeFileSync(process.cwd() + '/docker-compose.yaml', f)
f = `VIRTUAL_HOST=__domain__
LETSENCRYPT_HOST=__domain__
VIRTUAL_PORT=__port__
LETSENCRYPT_EMAIL=arancibiajav@gmail.com
NODE_ENV=production
PORT=__port__${argv.socket!==undefined?`
WRAPKEND_SOCKET_URI=__socket__`:""}
WRAPKEND_DB_URI=__db__
WRAPKEND_APIKEY=__apikey__
WRAPKEND_DB_DIRECT=__dbdirect__`
f = f
	.split('__domain__').join(argv.domain)
	.split('__port__').join(argv.port)
	.split('__db__').join(argv.db)
	.split('__socket__').join(argv.socket)
	.split('__apikey__').join(argv.apikey)
	.split('__dbdirect__').join(argv.dbdirect === undefined ? '0' : (argv.dbdirect?'1':argv.dbdirect))
sander.writeFileSync(process.cwd() + '/.env', f)
var json = JSON.parse(sander.readFileSync('./package.json').toString('utf-8'))
var start = `npx pm2 start ./app/index.js --name ${argv.name} && npx pm2 logs`
json.scripts.start = start
sander.writeFileSync(__dirname + '/package.json', JSON.stringify(json, null, 2))
process.exit(0)